package com.mashascent.httpModbus.action;

import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;

import com.mashascent.httpModbus.type.RegisterType;
import com.mashascent.httpModbus.response.HttpModbusResponse;

/**
 * When attempting to write a value to the modbus device you should
 * start with this object
 *
 * @author  Kelly Morphis
 */
public class WriteTransaction extends Transaction 
{
	public WriteTransaction(RegisterType register)
	{
		super(register);
	}
	
	public ModbusRequest getRequest()
	{
		return this.register.getWriteRequest();
	}
	
	public HttpModbusResponse generateResponse(ModbusResponse response)
	{
		return this.register.generateWriteResponse(response);
	}
}
