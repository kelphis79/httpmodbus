

package com.mashascent.httpModbus;

import java.util.Properties;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;

import javax.jws.WebParam;
import javax.jws.WebMethod;
import javax.jws.WebService;

import com.ghgande.j2mod.modbus.io.ModbusSerialTransaction;
import com.ghgande.j2mod.modbus.net.SerialConnection;
import com.ghgande.j2mod.modbus.util.SerialParameters;
import com.ghgande.j2mod.modbus.procimg.SimpleRegister;
import com.ghgande.j2mod.modbus.ModbusIOException;

import com.mashascent.httpModbus.device.Device;
import com.mashascent.httpModbus.response.HttpModbusBitResponse;
import com.mashascent.httpModbus.response.HttpModbusIntResponse;
import com.mashascent.httpModbus.response.HttpModbusFloatResponse;
import com.mashascent.httpModbus.type.Coil;
import com.mashascent.httpModbus.type.Single;
import com.mashascent.httpModbus.type.Multiple;
import com.mashascent.httpModbus.action.WriteTransaction;
import com.mashascent.httpModbus.action.ReadTransaction;

/**
 * This class is the API interface for the modbus service. There are several 
 * methods in this class that allow you to call exposed rest methods which will 
 * write or read registers from the associated modbus a device.
 * 
 * The methods of this class all return an object the extends an 
 * HttpModbusResponse object.
 *
 * @author  Kelly Morphis
 */
@Path("/")
@WebService
public class ModbusServer extends ModbusCore {
    private SerialConnection connection;
	private SerialParameters connectionParams;

	public ModbusServer() 
	{
		Device.getDevice().connect();
	}
	
	/**
	 * Sets a coil in the appropriate PLC with the value provided at the address 
	 * 
	 * @param  value  True or false.  This is a coil with is a binary value.
	 * @param  address	The PLC address which will be translated to the 
	 * Modbus address of address - 1. When referencing the modbus address the PLC 
	 * is 1 higher than the modbus address.
	 * @return      A HttpModbusBitResponse object
	 */
	@GET
	@Path("/writeBit/{value}/{address}")
    @Produces("application/json")
    public HttpModbusBitResponse writeBit(@PathParam("value") boolean value, @PathParam("address") int address)
    {
		ModbusSerialTransaction modbusTransaction;
		HttpModbusBitResponse result = new HttpModbusBitResponse();
		
		Coil coil = new Coil(address);
		coil.setNewValue(value);
		WriteTransaction transaction = new WriteTransaction(coil);
		
		this.getLogger().info("Writing coil at address ("+Integer.toString(coil.getAddress())+"): "+(coil.getNewValue()?"1":"0"));
		
		try {
			result = (HttpModbusBitResponse) Device.getDevice().processTransaction(transaction);
		} catch (Exception e) {
			this.getLogger().error("Error occured while processing bit write. " + e.getMessage());
			result.setErrorMessage(e.getMessage());
		}
		
		return result;
    }
	
	/**
	 * Sets an integer register in the appropriate PLC with the value provided at the address 
	 * 
	 * @param  value  A signed integer that will be stored in the register at address
	 * @param  address	The PLC address which will be translated to the 
	 * Modbus address of address - 1. When referencing the modbus address the PLC 
	 * is 1 higher than the modbus address.
	 * @return      A HttpModbusIntResponse object
	 */
	@GET
	@Path("/writeInt/{value}/{address}")
    @Produces("application/json")
	public HttpModbusIntResponse writeInt(@PathParam("value") int value, @PathParam("address") int address)
    {
		ModbusSerialTransaction modbusTransaction;
		HttpModbusIntResponse result = new HttpModbusIntResponse();

		Single singleRegister = new Single(address);
		singleRegister.setNewValue(value);
		WriteTransaction transaction = new WriteTransaction(singleRegister);
		
		this.getLogger().info("Writing register at address ("+Integer.toString(singleRegister.getAddress())+"): "+
								Integer.toString(singleRegister.getNewValue()));
		
		try {
			result = (HttpModbusIntResponse) Device.getDevice().processTransaction(transaction);
		} catch (Exception e) {
			this.getLogger().error("Error occured while processing single register write. " + e.getMessage());
			result.setErrorMessage(e.getMessage());
		}
		
		return result;
    }
    
	/**
	 * Sets a multiple register in the appropriate PLC with the value provided starting at address 
	 * 
	 * @param  value  A signed floting point integer that will be stored in the register at address
	 * @param  address	The PLC address which will be translated to the 
	 * Modbus address of address - 1. When referencing the modbus address the PLC 
	 * is 1 higher than the modbus address.
	 * @return      A HttpModbusFloatResponse object
	 */
	@GET
	@Path("/writeFloat/{value}/{address}")
    @Produces("application/json")
	public HttpModbusFloatResponse writeFloat(@PathParam("value") float value, @PathParam("address") int address)
    {
		ModbusSerialTransaction modbusTransaction;
		HttpModbusFloatResponse result = new HttpModbusFloatResponse();
		
		Multiple multipleRegister = new Multiple(address);
		multipleRegister.setNewValue(value);
		WriteTransaction transaction = new WriteTransaction(multipleRegister);
		
		this.getLogger().info("Writing multiple-register at address ("+Integer.toString(multipleRegister.getAddress())+"): "+
								multipleRegister.getNewValue());
		
		try {
			result = (HttpModbusFloatResponse) Device.getDevice().processTransaction(transaction);
		} catch (Exception e) {
			this.getLogger().error("Error occured while processing multi register write. " + e.getMessage());
			result.setErrorMessage(e.getMessage());
		}
		
		return result;
    }

	/**
	 * Reads a coil in the appropriate PLC and returns the value stored at the address 
	 * 
	 * @param  address	The PLC address which will be translated to the 
	 * Modbus address of address - 1. When referencing the modbus address the PLC 
	 * is 1 higher than the modbus address.
	 * @return      A HttpModbusBitResponse object
	 */
	@GET
	@Path("/readBit/{address}")
    @Produces("application/json")
    public HttpModbusBitResponse readBit(@PathParam("address") int address) throws Exception
    {
		ModbusSerialTransaction modbusTransaction;
		HttpModbusBitResponse result = new HttpModbusBitResponse();
		
		Coil coil = new Coil(address);
		ReadTransaction transaction = new ReadTransaction(coil);
		
		this.getLogger().info("Reading coil at address:"+Integer.toString(coil.getAddress()));
		
		try {
			result = (HttpModbusBitResponse) Device.getDevice().processTransaction(transaction);
		} catch (Exception e) {
			this.getLogger().error("Error occured while processing bit read. " + e.getMessage());
			result.setErrorMessage(e.getMessage());
		}
		
		return result;
    }
	
	/**
	 * Reads a single register in the appropriate PLC and returns the value stored at the address 
	 * 
	 * @param  address	The PLC address which will be translated to the 
	 * Modbus address of address - 1. When referencing the modbus address the PLC 
	 * is 1 higher than the modbus address.
	 * @return      A HttpModbusIntResponse object
	 */
	@GET
	@Path("/readInt/{address}")
    @Produces("application/json")
    public HttpModbusIntResponse readInt(@PathParam("address") int address) throws Exception
    {	
		ModbusSerialTransaction modbusTransaction;
		HttpModbusIntResponse result = new HttpModbusIntResponse();
		
		Single singleRegister = new Single(address);
		ReadTransaction transaction = new ReadTransaction(singleRegister);
		
		this.getLogger().info("Reading register at address:"+Integer.toString(singleRegister.getAddress()));
		
		try {
			result = (HttpModbusIntResponse) Device.getDevice().processTransaction(transaction);
		} catch (Exception e) {
			this.getLogger().error("Error occured while processing single register read. " + e.getMessage());
			result.setErrorMessage(e.getMessage());
		}
		
		return result;
    }	
	
	/**
	 * Reads multiple registers in the appropriate PLC and returns the value stored at the address 
	 * 
	 * @param  address	The PLC address which will be translated to the 
	 * Modbus address of address - 1. When referencing the modbus address the PLC 
	 * is 1 higher than the modbus address.
	 * @return      A HttpModbusFloatResponse object
	 */
	@GET
	@Path("/readFloat/{address}")
    @Produces("application/json")
	public HttpModbusFloatResponse readFloat(@PathParam("address") int address)
    {
		ModbusSerialTransaction modbusTransaction;
		HttpModbusFloatResponse result = new HttpModbusFloatResponse();
		
		Multiple multipleRegister = new Multiple(address);
		ReadTransaction transaction = new ReadTransaction(multipleRegister);
		
		this.getLogger().info("Reading multiple-register at address ("+Integer.toString(multipleRegister.getAddress())+")");
		
		try {
			result = (HttpModbusFloatResponse) Device.getDevice().processTransaction(transaction);
		} catch (Exception e) {
			this.getLogger().error("Error occured while processing multi register read. " + e.getMessage());
			result.setErrorMessage(e.getMessage());
		}
		
		return result;
    }
}