package com.mashascent.httpModbus.type;

import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.msg.WriteMultipleRegistersRequest;
import com.ghgande.j2mod.modbus.msg.WriteMultipleRegistersResponse;
import com.ghgande.j2mod.modbus.msg.ReadMultipleRegistersRequest;
import com.ghgande.j2mod.modbus.msg.ReadMultipleRegistersResponse;
import com.ghgande.j2mod.modbus.procimg.SimpleRegister;
import com.ghgande.j2mod.modbus.procimg.InputRegister;

import com.mashascent.httpModbus.response.HttpModbusFloatResponse;
import com.mashascent.httpModbus.response.HttpModbusResponse;

import java.lang.Float;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * This class represent the multiple register modbus address. A float address
 * is typically a 4 byte address.
 * There is no limits on the values that can be set. The PLC will prevent values 
 * outside the register abilities.
 *
 * @author  Kelly Morphis
 */
public class Multiple extends RegisterType<Float>
{
	public Multiple(int address)
	{
		super(address);
	}
	
	/**
	 * Create the read request for the multiple register address
	 * 
	 * @return		A ReadMultipleRegistersRequest object
	 */	
	public ReadMultipleRegistersRequest getReadRequest()
	{
		return new ReadMultipleRegistersRequest(this.getAddress(), 2);
	}
	
	/**
	 * Create the read request for the multiple register address
	 * 
	 * @return		A HttpModbusResponse object
	 */	
	public HttpModbusResponse generateReadResponse(ModbusResponse modbusResponse)
	{
		ReadMultipleRegistersResponse castedModbusResponse = (ReadMultipleRegistersResponse) modbusResponse;
		HttpModbusFloatResponse response = new HttpModbusFloatResponse(this.getAddress());
		SimpleRegister[] registers = new SimpleRegister[2];
		float valueFromResponse;

		int result = 0;
		int position = 0;
		byte[] chunk;
		byte[] allBytes = new byte[registers.length*2];
	
		for (int i=registers.length-1; i>=0; i--) {
			chunk = castedModbusResponse.getRegister(i).toBytes();
			System.arraycopy(chunk, 0, allBytes, position, chunk.length);
			position += chunk.length;
		}
		
		ByteBuffer buf = ByteBuffer.wrap(allBytes);
		valueFromResponse = buf.getFloat();

		response.setResult(valueFromResponse);
		this.getLogger().info("Succesfully read value of register at address ("+Integer.toString(this.getAddress())+"): "+Float.toString(valueFromResponse));
		
		return response;
	}
	
	/**
	 * Create the write request for the multiple register address
	 * 
	 * @return		A WriteMultipleRegistersRequest object
	 */	
	public WriteMultipleRegistersRequest getWriteRequest()
	{	
    	SimpleRegister[] registers = new SimpleRegister[2];
		
		byte[] bytes = java.nio.ByteBuffer.allocate(4).putFloat(this.getNewValue()).array();
	    
		registers[1] = new SimpleRegister(bytes[0],bytes[1]);
		registers[0] = new SimpleRegister(bytes[2],bytes[3]);
		
		return new WriteMultipleRegistersRequest(this.getAddress(), registers);
	}
	
	/**
	 * Create the write response for the multiple register address
	 * 
	 * @return		A HttpModbusResponse object
	 */	
	public HttpModbusResponse generateWriteResponse(ModbusResponse modbusResponse)
	{
		WriteMultipleRegistersResponse castedModbusResponse = (WriteMultipleRegistersResponse) modbusResponse;
		HttpModbusFloatResponse response = new HttpModbusFloatResponse(this.getAddress());
		float addressFromResponse;
		
		//for some reason this object doesnt return the value written so the only sanity check you can do is to 
		//check the address that was written. Not advisable to perform another read hear since there is no 
		//guarantee that another process hasnt written to the same register
		int writtenReference = castedModbusResponse.getReference();
		
		if (writtenReference == this.getAddress()) {
			this.getLogger().info("Successfully modified register at address ("+Integer.toString(this.getAddress())+")");
			this.setLastValue(this.getNewValue());
    	} else {
    		response.setErrorIndicator();
    		this.getLogger().error("Register not updated. Address "+Integer.toString(writtenReference)+
    									" returned when expecting ("+Integer.toString(this.getAddress())+")");
    	}
		
		return response;
	}

}
