package com.mashascent.httpModbus.type;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mashascent.httpModbus.response.HttpModbusBitResponse;
import com.mashascent.httpModbus.type.Coil;

import com.ghgande.j2mod.modbus.Modbus;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.msg.ReadCoilsResponse;
import com.ghgande.j2mod.modbus.msg.ReadCoilsRequest;
import com.ghgande.j2mod.modbus.msg.WriteCoilResponse;
import com.ghgande.j2mod.modbus.msg.WriteCoilRequest;


public class CoilTest 
{
	@Test
	public void testGetReadRequest() 
	{
		Coil coil = new Coil(1);
		ReadCoilsRequest request = coil.getReadRequest();
		
		assertTrue(request instanceof ReadCoilsRequest);
		assertEquals(0, request.getReference());
		assertEquals(1, request.getBitCount());
	}
	
	@Test(expected = IllegalArgumentException.class) 
	public void testInvalidCoilAddress() 
	{
		Coil coil = new Coil(0);
	}
	
	@Test
	public void testGenerateReadResponse()
	{
		ReadCoilsResponse responseStub = new ReadCoilsResponse(1);
		HttpModbusBitResponse testResponse = null;
		Coil coil = new Coil(1);
		
		//set a fake coil to true
		responseStub.setCoilStatus(0,true);

		testResponse = (HttpModbusBitResponse)coil.generateReadResponse(responseStub);
		
		assertTrue(testResponse instanceof HttpModbusBitResponse);
		assertTrue(testResponse.getResult());
	}
	
	@Test
	public void testGetWriteRequest() 
	{
		Coil coil = new Coil(1);
		coil.setNewValue(true);
		WriteCoilRequest request = coil.getWriteRequest();
		
		assertTrue(request instanceof WriteCoilRequest);
		assertEquals(0, request.getReference());
		assertTrue(request.getCoil());
	}
	
	@Test
	public void testGenerateWriteResponse()
	{
		WriteCoilResponse responseStub = new WriteCoilResponse(0,true);
		HttpModbusBitResponse testResponse = null;
		Coil coil = new Coil(1);
		coil.setNewValue(true);
		
		//set a fake coild to true
		responseStub.setCoil(true);

		testResponse = (HttpModbusBitResponse)coil.generateWriteResponse(responseStub);
		
		assertTrue(testResponse instanceof HttpModbusBitResponse);
		assertEquals(true, testResponse.getResult());
	}
	
	@Test
	public void testGenerateWriteResponseValuesDontMatch()
	{
		WriteCoilResponse responseStub = new WriteCoilResponse(0,true);
		HttpModbusBitResponse testResponse = null;
		Coil coil = new Coil(1);
		coil.setNewValue(true);
		
		//set a fake coild to true
		responseStub.setCoil(false);

		testResponse = (HttpModbusBitResponse)coil.generateWriteResponse(responseStub);
		assertTrue(testResponse.hadErrorsOccured());
	}
}
