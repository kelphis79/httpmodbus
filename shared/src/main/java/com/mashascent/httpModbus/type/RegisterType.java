package com.mashascent.httpModbus.type;

import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.io.ModbusSerialTransaction;

import com.mashascent.httpModbus.ModbusCore;
import com.mashascent.httpModbus.response.HttpModbusBitResponse;
import com.mashascent.httpModbus.response.HttpModbusResponse;

/**
 * This class represent the base register modbus class.
 *
 * @author  Kelly Morphis
 */
public abstract class RegisterType<T> extends ModbusCore
{
	private int address;
	private T newValue;
	private T lastValue;
	
	public RegisterType(int address)
	{
		//modbus range indexes start at 1 but memory is in the zero position
		if (address <= 0) {
			throw new IllegalArgumentException("Address less than 1.");
		}
		this.address = address-1;
	}
	
	public int getAddress()
	{
		return this.address;
	}
	
	public void setAddress(int address)
	{
		this.address = address;
	}
	
	public T getNewValue()
	{
		return this.newValue;
	}
	
	public void setNewValue(T value)
	{
		this.newValue = value;
	}
	
	public T getLastValue()
	{
		return this.lastValue;
	}
	
	public void setLastValue(T value)
	{
		this.lastValue = value;
	}
	
	public abstract ModbusRequest getWriteRequest();
	public abstract ModbusRequest getReadRequest();
	public abstract HttpModbusResponse generateWriteResponse(ModbusResponse modbusReponse);
	public abstract HttpModbusResponse generateReadResponse(ModbusResponse modbusReponse);
}
