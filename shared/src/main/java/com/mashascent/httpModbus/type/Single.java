package com.mashascent.httpModbus.type;

import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.msg.WriteSingleRegisterRequest;
import com.ghgande.j2mod.modbus.msg.WriteSingleRegisterResponse;
import com.ghgande.j2mod.modbus.msg.ReadInputRegistersRequest;
import com.ghgande.j2mod.modbus.msg.ReadInputRegistersResponse;
import com.ghgande.j2mod.modbus.io.ModbusSerialTransaction;
import com.ghgande.j2mod.modbus.procimg.SimpleRegister;

import com.mashascent.httpModbus.action.WriteTransaction;
import com.mashascent.httpModbus.response.HttpModbusResponse;
import com.mashascent.httpModbus.response.HttpModbusIntResponse;

/**
 * This class represent the single register modbus address. A single address
 * is typically a 16 bit integer address but can reference an 8 bit short address.
 * There is no limits on the values that can be set. The PLC will prevent values 
 * outside the register side.
 *
 * @author  Kelly Morphis
 */
public class Single extends RegisterType <Integer>
{	
	public Single(int address)
	{
		super(address);
	}
	
	/**
	 * Create the read request for the single register address
	 * 
	 * @return		A ReadInputRegistersRequest object
	 */	
	public ReadInputRegistersRequest getReadRequest()
	{
		return  new ReadInputRegistersRequest(this.getAddress(), 1);
	}
	
	/**
	 * Create the read response for the multiple register address
	 * 
	 * @return		A HttpModbusResponse object
	 */	
	public HttpModbusResponse generateReadResponse(ModbusResponse modbusResponse)
	{
		ReadInputRegistersResponse castedModbusResponse = (ReadInputRegistersResponse) modbusResponse;
		HttpModbusIntResponse response = new HttpModbusIntResponse(this.getAddress());
		
		response.setResult(castedModbusResponse.getRegisterValue(0));
		if (castedModbusResponse.getWordCount() > 0) {
			this.getLogger().info("Value of register at address ("+Integer.toString(this.getAddress())+"): "+Integer.toString(castedModbusResponse.getRegisterValue(0)));
    	} else {
    		response.setErrorIndicator();
    		this.getLogger().error("Unable to read register at address ("+Integer.toString(this.getAddress())+")");
    	}
		
		return response;
	}
	
	/**
	 * Create the write request for the single register address
	 * 
	 * @return		A WriteSingleRegisterRequest object
	 */	
	public WriteSingleRegisterRequest getWriteRequest()
	{
		return new WriteSingleRegisterRequest(this.getAddress(), new SimpleRegister(this.getNewValue()));
	}
	
	/**
	 * Create the write response for the multiple register address
	 * 
	 * @return		A HttpModbusResponse object
	 */	
	public HttpModbusResponse generateWriteResponse(ModbusResponse modbusResponse)
	{
		WriteSingleRegisterResponse castedModbusResponse = (WriteSingleRegisterResponse) modbusResponse;
		HttpModbusIntResponse response = new HttpModbusIntResponse(this.getAddress());
		
		response.setResult(castedModbusResponse.getRegisterValue());
		if (castedModbusResponse.getRegisterValue() == this.getNewValue()) {
			this.getLogger().info("Successfully modified register at address ("+Integer.toString(this.getAddress())+"): "+ castedModbusResponse.getRegisterValue());
			this.setLastValue(this.getNewValue());
    	} else {
    		response.setErrorIndicator();
    		this.getLogger().error("Register not updated. Value of "+Integer.toString(castedModbusResponse.getRegisterValue())+
    									" read when expecting ("+Integer.toString(this.getNewValue())+")");
    	}
		
		return response;
	}
}
