package com.mashascent.httpModbus.action;

import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.mashascent.httpModbus.response.HttpModbusResponse;
import com.mashascent.httpModbus.type.RegisterType;

/**
 * When attempting to read a value to the modbus device you should
 * start with this object
 *
 * @author  Kelly Morphis
 */
public class ReadTransaction extends Transaction 
{
	public ReadTransaction(RegisterType register)
	{
		super(register);
	}
	
	/**
	 * Get the appropriate request object for the register type
	 * 
	 * @return	ModbusRequest	A ModbusRequest object
	 */	
	public ModbusRequest getRequest()
	{
		return this.register.getReadRequest();
	}
	
	/**
	 * Get the appropriate response object for the register type
	 * 
	 * @return	HttpModbusResponse	A HttpModbusResponse object
	 */	
	public HttpModbusResponse generateResponse(ModbusResponse response)
	{
		return this.register.generateReadResponse(response);
	}
}