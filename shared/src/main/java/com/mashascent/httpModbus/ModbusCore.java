package com.mashascent.httpModbus;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Base class for the service. Anything that is global to the application can
 * be found here
 *
 * @author  Kelly Morphis
 */
public abstract class ModbusCore 
{
	private static Logger logger;
	
	public ModbusCore()
	{
		
	}
	
	public static Logger getLogger()
	{
		if (logger == null) {
			logger = LogManager.getLogger(ModbusServer.class.getName());
		}
		
		return logger;
	}
}
