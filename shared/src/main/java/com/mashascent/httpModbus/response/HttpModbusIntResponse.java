package com.mashascent.httpModbus.response;

/**
 * This will be returned to calls to the modbus service to indicate
 * results for a single register modification.
 *
 * @author  Kelly Morphis
 */
public class HttpModbusIntResponse extends HttpModbusResponse<Integer> 
{	
	public HttpModbusIntResponse() {}
	
	public HttpModbusIntResponse(int address)
	{
		super(address);
	}
}
