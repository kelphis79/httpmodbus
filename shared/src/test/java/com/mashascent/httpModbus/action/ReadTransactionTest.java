package com.mashascent.httpModbus.type;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mashascent.httpModbus.action.ReadTransaction;
import com.mashascent.httpModbus.response.HttpModbusBitResponse;
import com.mashascent.httpModbus.response.HttpModbusResponse;
import com.mashascent.httpModbus.type.Coil;

import com.ghgande.j2mod.modbus.Modbus;
import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.msg.ReadCoilsResponse;
import com.ghgande.j2mod.modbus.msg.ReadCoilsRequest;

public class ReadTransactionTest 
{
	private ReadTransaction transaction;
	
	@Before
	public void setUp() throws Exception 
	{
		Coil coil = new Coil(1);
		this.transaction = new ReadTransaction(coil);
	}
	
	@Test
	public void testGetRequest() 
	{
		ModbusRequest request = this.transaction.getRequest();
		
		assertTrue(request instanceof ReadCoilsRequest);
	}
	
	@Test
	public void testGenerateResponse() 
	{
		ReadCoilsResponse responseStub = new ReadCoilsResponse(1);
		HttpModbusResponse request = this.transaction.generateResponse(responseStub);
		
		assertTrue(request instanceof HttpModbusResponse);
	}
}
