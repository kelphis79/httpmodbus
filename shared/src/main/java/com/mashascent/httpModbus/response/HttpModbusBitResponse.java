package com.mashascent.httpModbus.response;

/**
 * This will be returned to calls to the modbus service to indicate
 * results for a coil register modification.
 *
 * @author  Kelly Morphis
 */
public class HttpModbusBitResponse extends HttpModbusResponse<Boolean> 
{	
	public HttpModbusBitResponse() {}
	
	public HttpModbusBitResponse(int address)
	{
		super(address);
	}
}
