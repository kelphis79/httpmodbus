import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mashascent.httpModbus.action.WriteTransaction;
import com.mashascent.httpModbus.response.HttpModbusBitResponse;
import com.mashascent.httpModbus.response.HttpModbusResponse;
import com.mashascent.httpModbus.type.Coil;

import com.ghgande.j2mod.modbus.Modbus;
import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.msg.WriteCoilResponse;
import com.ghgande.j2mod.modbus.msg.WriteCoilRequest;

public class WriteTransactionTest 
{
	private WriteTransaction transaction;
	
	@Before
	public void setUp() throws Exception 
	{
		Coil coil = new Coil(1);
		coil.setNewValue(true);
		this.transaction = new WriteTransaction(coil);
	}
	
	@Test
	public void testGetRequest() 
	{
		ModbusRequest request = this.transaction.getRequest();
		
		assertTrue(request instanceof WriteCoilRequest);
	}
	
	@Test
	public void testGenerateResponse() 
	{
		WriteCoilResponse responseStub = new WriteCoilResponse(0,true);
		HttpModbusResponse request = this.transaction.generateResponse(responseStub);
		
		assertTrue(request instanceof HttpModbusResponse);
	}
}
