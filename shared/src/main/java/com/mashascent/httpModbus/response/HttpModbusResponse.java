package com.mashascent.httpModbus.response;

import javax.ws.rs.core.Application;

/**
 * The HttpModbus class represents the base response for any call to any
 * method of the modbus service
 *
 * @author  Kelly Morphis
 */
public abstract class HttpModbusResponse<T> 
{
	private int address;
	private boolean errorOccured = false;
	private int test;
	private T result;
	
	private String errorMessage;
	
	public HttpModbusResponse()
	{
		
	}
	
	public HttpModbusResponse(int address)
	{
		this.address = address;
	}
	
	public void setErrorMessage(String message)
	{
		this.errorMessage = message;
		this.setErrorIndicator();
	}
	
	public void setErrorIndicator()
	{
		this.errorOccured = true;
	}
	
	public boolean hadErrorsOccured()
	{
		return this.errorOccured;
	}
	
	public String getErrorMessage()
	{
		return this.errorMessage;
	}
	
	public int getAddress()
	{
		return this.address;
	}
	
	public void setAddress(int address)
	{
		//PLC values are 1 higher than modbus values
		this.address = address+1;
	}
	
	public void setResult(T result) 
	{
		this.result = result;
	}
	
	public T getResult()
	{
		return this.result;
	}
}
