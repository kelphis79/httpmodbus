package com.mashascent.httpModbus.type;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mashascent.httpModbus.response.HttpModbusIntResponse;

import com.ghgande.j2mod.modbus.Modbus;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.msg.ReadInputRegistersResponse;
import com.ghgande.j2mod.modbus.msg.ReadInputRegistersRequest;
import com.ghgande.j2mod.modbus.msg.WriteSingleRegisterResponse;
import com.ghgande.j2mod.modbus.msg.WriteSingleRegisterRequest;
import com.ghgande.j2mod.modbus.procimg.SimpleRegister;


public class SingleTest 
{
	@Test
	public void testGetReadRequest() 
	{
		Single register = new Single(1);
		ReadInputRegistersRequest request = register.getReadRequest();
		
		assertTrue(request instanceof ReadInputRegistersRequest);
		assertEquals(0, request.getReference());
		assertEquals(1, request.getWordCount());
	}
	
	@Test(expected = IllegalArgumentException.class) 
	public void testInvalidSingleAddress() 
	{
		Single register = new Single(0);
	}
	
	@Test
	public void testGenerateReadResponse()
	{
		ReadInputRegistersResponse responseStub;
		HttpModbusIntResponse testResponse = null;
		Single register = new Single(1);
		SimpleRegister[] registers = new SimpleRegister[1];
		Integer testValue = 5;
		
		registers[0] = new SimpleRegister(testValue);
		responseStub = new ReadInputRegistersResponse(registers);

		testResponse = (HttpModbusIntResponse)register.generateReadResponse(responseStub);
		
		assertTrue(testResponse instanceof HttpModbusIntResponse);
		assertEquals(testValue, testResponse.getResult());
	}
	
	@Test
	public void testGetWriteRequest() 
	{
		Single register = new Single(1);
		register.setNewValue(5);
		WriteSingleRegisterRequest request = register.getWriteRequest();
		
		assertTrue(request instanceof WriteSingleRegisterRequest);
		assertEquals(0, request.getReference());
		assertEquals(5, request.getRegister().getValue());
	}
	
	@Test
	public void testGenerateWriteResponse()
	{
		Integer testValue = 5;
		WriteSingleRegisterResponse responseStub = new WriteSingleRegisterResponse(0,testValue);
		HttpModbusIntResponse testResponse = null;
		Single register = new Single(1);
		register.setNewValue(testValue);
		
		testResponse = (HttpModbusIntResponse)register.generateWriteResponse(responseStub);
		
		assertEquals(testValue, testResponse.getResult());
		assertFalse(testResponse.hadErrorsOccured());
	}
	
	@Test
	public void testGenerateWriteResponseValuesDontMatch()
	{
		Integer testValue = 5;
		WriteSingleRegisterResponse responseStub = new WriteSingleRegisterResponse(0,testValue);
		HttpModbusIntResponse testResponse = null;
		Single register = new Single(1);
		
		//set the desired value of the register to something differnt than whats in the response
		register.setNewValue(testValue+1);
		
		testResponse = (HttpModbusIntResponse)register.generateWriteResponse(responseStub);
		
		assertTrue(testResponse.hadErrorsOccured());
	}
}
