package com.mashascent.httpModbus.type;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mashascent.httpModbus.ModbusCore;
import com.mashascent.httpModbus.response.HttpModbusFloatResponse;
import com.mashascent.httpModbus.type.Multiple;

import com.ghgande.j2mod.modbus.Modbus;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.msg.ReadMultipleRegistersResponse;
import com.ghgande.j2mod.modbus.msg.ReadMultipleRegistersRequest;
import com.ghgande.j2mod.modbus.msg.WriteMultipleRegistersResponse;
import com.ghgande.j2mod.modbus.msg.WriteMultipleRegistersRequest;
import com.ghgande.j2mod.modbus.procimg.SimpleRegister;


public class MultipleTest 
{
	@Test
	public void testGetReadRequest() 
	{
		Multiple multiple = new Multiple(1);
		ReadMultipleRegistersRequest request = multiple.getReadRequest();
		
		assertTrue(request instanceof ReadMultipleRegistersRequest);
		assertEquals(0, request.getReference());
		assertEquals(2, request.getWordCount());
	}
	
	@Test(expected = IllegalArgumentException.class) 
	public void testInvalidFloatAddress() 
	{
		Multiple multiple = new Multiple(0);
	}
	
	@Test
	public void testGenerateReadResponse()
	{
		ReadMultipleRegistersResponse responseStub;
		HttpModbusFloatResponse testResponse = null;
		Multiple multiple = new Multiple(1);
		SimpleRegister[] registers = new SimpleRegister[2];
		
		//thats amazing! I've got the same combination on my luggage!
		registers[0] = new SimpleRegister(20480);
		registers[1] = new SimpleRegister(17562);
		
		responseStub = new ReadMultipleRegistersResponse(registers);

		testResponse = (HttpModbusFloatResponse)multiple.generateReadResponse(responseStub);
		
		assertTrue(testResponse instanceof HttpModbusFloatResponse);
		assertEquals(1234.5, testResponse.getResult(), 0.0);
	}
	
	@Test
	public void testGetWriteRequest() 
	{
		Multiple register = new Multiple(1);
		register.setNewValue(1234.5f);
		WriteMultipleRegistersRequest request = register.getWriteRequest();
		
		assertTrue(request instanceof WriteMultipleRegistersRequest);
		assertEquals(0, request.getReference());
		assertEquals(20480, request.getRegister(0).getValue());
		assertEquals(17562, request.getRegister(1).getValue());
	}
	
	@Test
	public void testGenerateWriteResponse()
	{
		WriteMultipleRegistersResponse responseStub = new WriteMultipleRegistersResponse(0,2);
		HttpModbusFloatResponse testResponse = null;
		Multiple register = new Multiple(1);

		testResponse = (HttpModbusFloatResponse)register.generateWriteResponse(responseStub);
		
		assertFalse(testResponse.hadErrorsOccured());
	}
	
	@Test
	public void testGenerateWriteResponseInvalidReferenceReturned()
	{
		WriteMultipleRegistersResponse responseStub = new WriteMultipleRegistersResponse(0,2);
		HttpModbusFloatResponse testResponse = null;
		
		//This would be the original reference. The above registerResponse referenced position 0
		Multiple register = new Multiple(5);
		
		testResponse = (HttpModbusFloatResponse)register.generateWriteResponse(responseStub);
		
		assertTrue(testResponse.hadErrorsOccured());
	}
}
