package com.mashascent.httpModbus.response;

/**
 * This will be returned to calls to the modbus service to indicate
 * results for a floating point value modificiation for a specific register.
 *
 * @author  Kelly Morphis
 */
public class HttpModbusFloatResponse extends HttpModbusResponse<Float> 
{
	public HttpModbusFloatResponse() {}
	
	public HttpModbusFloatResponse(int address)
	{
		super(address);
	}
}
