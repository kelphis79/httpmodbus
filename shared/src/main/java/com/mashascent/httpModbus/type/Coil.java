package com.mashascent.httpModbus.type;

import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.msg.WriteCoilRequest;
import com.ghgande.j2mod.modbus.msg.WriteCoilResponse;
import com.ghgande.j2mod.modbus.msg.ReadCoilsRequest;
import com.ghgande.j2mod.modbus.msg.ReadCoilsResponse;
import com.ghgande.j2mod.modbus.io.ModbusSerialTransaction;

import com.mashascent.httpModbus.action.WriteTransaction;
import com.mashascent.httpModbus.response.HttpModbusResponse;
import com.mashascent.httpModbus.response.HttpModbusBitResponse;

/**
 * This class represent the coil register modbus address. A coil address
 * is a 1 bit address.
 *
 * @author  Kelly Morphis
 */
public class Coil extends RegisterType <Boolean>
{	
	public Coil(int address) {
		super(address);
	}
	
	/**
	 * Create the read request for the coil
	 * 
	 * @return		A ReadCoilsRequest object
	 */	
	public ReadCoilsRequest getReadRequest()
	{
		return new ReadCoilsRequest(this.getAddress(), 1);
	}
	
	/**
	 * Create the read response object for the coil. 
	 * 
	 * @return		A HttpModbusResponse object
	 */	
	public HttpModbusResponse generateReadResponse(ModbusResponse modbusResponse)
	{
		ReadCoilsResponse castedModbusResponse = (ReadCoilsResponse) modbusResponse;
		HttpModbusBitResponse response = new HttpModbusBitResponse(this.getAddress());
		
		response.setResult(castedModbusResponse.getCoilStatus(0));
		this.getLogger().info("Value of coil at address ("+Integer.toString(this.getAddress())+"): "+(castedModbusResponse.getCoilStatus(0)?1:0));
		
		return response;
	}
	
	/**
	 * Create the write request for the coil
	 * 
	 * @return		A WriteCoilRequest object
	 */	
	public WriteCoilRequest getWriteRequest()
	{
		return new WriteCoilRequest(this.getAddress(), this.getNewValue());
	}
	
	/**
	 * Create the write response object for the coil. 
	 * 
	 * @return		A HttpModbusResponse object
	 */	
	public HttpModbusResponse generateWriteResponse(ModbusResponse modbusResponse)
	{
		WriteCoilResponse castedModbusResponse = (WriteCoilResponse) modbusResponse;
		HttpModbusBitResponse response = new HttpModbusBitResponse(this.getAddress());
		
		response.setResult(castedModbusResponse.getCoil());
		if (castedModbusResponse.getCoil() == this.getNewValue()) {
			this.getLogger().info("Successfully modified coil at address ("+Integer.toString(this.getAddress())+"): "+(castedModbusResponse.getCoil()?"1":"0"));
			this.setLastValue(this.getNewValue());
    	} else {
    		response.setErrorIndicator();
    		this.getLogger().error("Coil not updated. Value of "+(castedModbusResponse.getCoil()?"1":"0")+" read when expecting ("+(this.getNewValue()?"1":"0")+")");
    	}
		
		return response;
	}
}
