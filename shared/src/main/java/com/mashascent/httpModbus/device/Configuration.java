package com.mashascent.httpModbus.device;

import com.ghgande.j2mod.modbus.ModbusIOException;
import com.ghgande.j2mod.modbus.io.ModbusSerialTransaction;
import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.util.SerialParameters;

import com.mashascent.httpModbus.ModbusCore;
import com.mashascent.httpModbus.type.RegisterType;

import java.lang.Exception;
import java.lang.IllegalArgumentException;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Any logic responsible for the handling of the configuration file.
 *
 * @author  Kelly Morphis
 */
public class Configuration extends ModbusCore
{
	private String portName;
	private Integer baudRate;
	private Integer dataBits;
	private String parity;
	private Integer stopBits;
	private String encoding;
	private boolean echo;
	
	public Configuration() {}

	private InputStream getFileInputStream() throws FileNotFoundException
	{
		String propFileName = "/etc/mashascent/httpModbus/config.properties";
		InputStream inputStream;
		
		this.getLogger().debug("Opening config file: "+propFileName);
		
		try {
			
			inputStream = new FileInputStream(propFileName);
		} catch (FileNotFoundException e) {
			this.getLogger().error("config.properties file not found. "+e.getMessage());
			throw e;
		}
		
		if (inputStream == null) {
			throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
		}
		return inputStream;
	}
	
	private void closeFileInputStream(InputStream inputStream)
	{
		try {
			inputStream.close();
		} catch (IOException e) {
			this.getLogger().error("Problem while attempting to close config.properties file. "+e.getMessage());
		}
	}
	
	/**
	 * Load the config file for the device
	 * 
	 * @return	boolean	A flag indicating if the config file was loaded without error
	 */	
	public boolean loadConfig()
	{
		InputStream inputStream = null;
		
		try {
			inputStream = this.getFileInputStream();
			Properties prop = new Properties();
			
			this.getLogger().debug("Loading config file");
			
			prop.load(inputStream);
 
			this.getLogger().debug("Setting parameters from config file");
			this.portName = prop.getProperty("portname");
			this.getLogger().debug("Portname=" + prop.getProperty("portname"));
			
			this.baudRate = Integer.parseInt(prop.getProperty("baudrate"));
			this.getLogger().debug("BaudRate="+prop.getProperty("baudrate"));
			
			this.dataBits = Integer.parseInt(prop.getProperty("databits"));
			this.getLogger().debug("databits="+prop.getProperty("databits"));
			
			this.parity = prop.getProperty("parity");
			this.getLogger().debug("Parity="+prop.getProperty("parity"));
			
			this.stopBits = Integer.parseInt(prop.getProperty("stopbits"));
			this.getLogger().debug("stopbits="+prop.getProperty("stopbits"));
			
			this.encoding = prop.getProperty("encoding");
			this.getLogger().debug("Encoding="+prop.getProperty("encoding"));
			
			this.echo = prop.getProperty("echo")=="true"?true:false;
			this.getLogger().debug("Echo="+prop.getProperty("echo"));
		} catch (FileNotFoundException e) {
			this.getLogger().error("config.properties file not found. "+e.getMessage());
			return false;
		} catch (IOException e) {
			this.getLogger().error("Problem while attempting to read config.properties file. "+e.getMessage());
			return false;
		} finally {
			if (inputStream != null) {
				this.closeFileInputStream(inputStream);
			}
		}
		
		return true;
	}
	
	/**
	 * Process the transaction by sending the request to the device
	 *
	 * @return	SerialParameters	A SerialParameters object
	 * @throws	IllegalArgumentException	An expected value was not set in the config file
	 */	
	public SerialParameters getSerialParameters() throws IllegalArgumentException
	{
		SerialParameters connectionParameters = new SerialParameters();
		
		if (this.portName == null) {
			throw new IllegalArgumentException("Port name is not set for device configuration.");
		}
		connectionParameters.setPortName(this.portName);
		
		if (this.baudRate == null) {
			throw new IllegalArgumentException("Baud rate is not set for device configuration.");
		}
		connectionParameters.setBaudRate(this.baudRate);
		
		if (this.dataBits == null) {
			throw new IllegalArgumentException("Data bits is not set for device configuration.");
		}
		connectionParameters.setDatabits(this.dataBits);
		
		if (this.parity == null) {
			throw new IllegalArgumentException("parity is not set for device configuration.");
		}
		connectionParameters.setParity(this.parity);
		
		if (this.stopBits == null) {
			throw new IllegalArgumentException("Stop bits is not set for device configuration.");
		}
		connectionParameters.setStopbits(this.stopBits);
		connectionParameters.setEncoding(this.encoding);
		connectionParameters.setEcho(this.echo);
	
		return connectionParameters;
	}
}
