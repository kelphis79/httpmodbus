package com.mashascent.httpModbus.device;

import com.ghgande.j2mod.modbus.ModbusException;
import com.ghgande.j2mod.modbus.ModbusIOException;
import com.ghgande.j2mod.modbus.io.ModbusSerialTransaction;
import com.ghgande.j2mod.modbus.net.SerialConnection;
import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.util.SerialParameters;

import com.mashascent.httpModbus.ModbusCore;
import com.mashascent.httpModbus.type.RegisterType;
import com.mashascent.httpModbus.action.Transaction;
import com.mashascent.httpModbus.device.Configuration;
import com.mashascent.httpModbus.response.HttpModbusResponse;

import java.lang.Exception;

/**
 * Any functionality specific to the device; which is likely a PLC
 *
 * @author  Kelly Morphis
 */
public class Device extends ModbusCore
{
	private static Device singletonInstance = null; 
	private Configuration connectionConfig;
	private SerialConnection connection;
	
	private Device()
	{
	}
	
	public static Device getDevice()
	{
		if (singletonInstance == null) {
			singletonInstance = new Device();
		}
  
        return singletonInstance; 
	}
	
	/**
	 * Load the config and anything necessary to prepare communication for the device.
	 * 
	 * @return	boolean	A flag indicating if the config was loaded.
	 */	
	private boolean initialize()
	{
		this.connectionConfig = new Configuration();
		
		if (!this.connectionConfig.loadConfig()) {
			this.getLogger().error("Error occured while trying to load config.");
			return false;
		}
		this.getLogger().error("Successfully completed the config load.");
		return true;
	}
	
	/**
	 * Establish a connection to the device
	 * 
	 * @return	boolean	A flag indicating if the connection is established
	 */	
	public boolean connect() 
	{
		if (this.isConnected()) {
			return true;
		}
		
		this.initialize();
		
		SerialParameters connectionParameters;
		
		try {
			connectionParameters = this.connectionConfig.getSerialParameters();
		} catch (IllegalArgumentException e) {
			this.getLogger().error("Failed to connect to device. "+e.getMessage());
	    	return false;
		}
		
		
		this.getLogger().info("Connecting to USB device.");
		this.connection = new SerialConnection(connectionParameters);
		try {
			this.getLogger().info("Opening connection."); 
			this.connection.open();		
			
			this.getLogger().info("Connected successfully.");
		} catch (Exception e) {
			this.getLogger().error("Failed to connect to device. "+e.getMessage());
	    	return false;
		}
		
		return true;
	}
	
	/**
	 * Is the device connected?
	 * 
	 * @return	boolean	A flag indicating if the device is connected
	 */	
	private boolean isConnected() 
	{
		if (this.connection != null) {
			if (this.connection.isOpen()) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Close the connection to the device
	 */	
	private void disconnect() 
	{
		this.getLogger().info("Closing USB connection.");
		this.connection.close(); 
		this.getLogger().info("Closed USB connection.");
	}
	
	/**
	 * Process the transaction by sending the request to the device
	 * 
	 * @param	Transaction	
	 * @return	HttpModbusResponse	A HttpModbusResponse object
	 * @throws	ModbusException	TODO why does this happen
	 * @throws	Exception	The device is not connected. No transaction can be processed
	 */	
	public HttpModbusResponse processTransaction(Transaction transaction) throws ModbusException, Exception
	{
		if (!this.isConnected()) {
			throw new Exception("Modbus device is not connected.");
		}
		
		ModbusRequest request = transaction.getRequest();
		ModbusResponse modbusResponse = null;
		HttpModbusResponse result;
		ModbusSerialTransaction serialTransaction = new ModbusSerialTransaction(this.connection);
		serialTransaction.setRequest(request);
		request.setUnitID(1);
		request.setHeadless();
		
		//serialTransactions are synchronized in the execute method
		serialTransaction.execute();
		
		modbusResponse = serialTransaction.getResponse();
		
		result = transaction.generateResponse(modbusResponse);
		
		return result;
	}
}
