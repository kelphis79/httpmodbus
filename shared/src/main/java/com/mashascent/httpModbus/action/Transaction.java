package com.mashascent.httpModbus.action;

import com.ghgande.j2mod.modbus.ModbusIOException;
import com.ghgande.j2mod.modbus.io.ModbusSerialTransaction;
import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.net.SerialConnection;

import com.mashascent.httpModbus.ModbusCore;
import com.mashascent.httpModbus.response.HttpModbusResponse;
import com.mashascent.httpModbus.type.RegisterType;

import java.lang.Exception;

/**
 * When attempting to perform a read or write to the modbus device you 
 * should start with this object
 *
 * @author  Kelly Morphis
 */
public abstract class Transaction extends ModbusCore
{
	protected RegisterType register = null;
	private boolean transactionError = false;
	private String lastErrorMessage = "";
	
	public Transaction(RegisterType register)
	{
		this.register = register;
	}
	
	public abstract ModbusRequest getRequest();
	public abstract HttpModbusResponse generateResponse(ModbusResponse response);
	
	protected void clearTransactionData()
	{
		this.transactionError = false;
	}
	
	public boolean getTransactionError()
	{
		return this.transactionError;
	}
	
	protected void setTransactionError(boolean newValue)
	{
		this.transactionError = newValue;
	}
	
	public String getLastErrorMessage()
	{
		return this.lastErrorMessage;
	}
	
	protected void setLastErrorMessage(String newMessage)
	{
		this.lastErrorMessage = newMessage;
	}
}
